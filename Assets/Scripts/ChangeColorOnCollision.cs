using UnityEngine;

public class ChangeColorOnCollision : MonoBehaviour
{
    public GameObject audioPrefab;
    public static int Score;
    
    void Awake()
    {
        if (Score > 0)
            Score = 0;
    }
    private void OnCollisionEnter(Collision other)
    {
        Color newColor = other.transform.gameObject.GetComponent<MeshRenderer>().material.color;
        Instantiate(audioPrefab);
        gameObject.GetComponent<MeshRenderer>().material.color = newColor;
        if (gameObject.CompareTag("UnusedBlock"))
        {
            Score++;
            gameObject.tag = "UsedBlock";
        }
        Destroy(other.gameObject);
    }
}