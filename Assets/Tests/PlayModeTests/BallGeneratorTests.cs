using System.Collections;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests.PlayModeTests
{
    public class BallGeneratorTests
    {


        [UnityTest]
        public IEnumerator ShouldGenerateBallOnTouchInput()
        {
            string TestobjectName = "TestObject2";

            // Given 
            BallGenerator ballGenerator = new GameObject().AddComponent<BallGenerator>();
            
            GameObject ball = new GameObject(TestobjectName);
        
            ballGenerator.ballPrefab = ball;
            ballGenerator.arCamera = new GameObject().AddComponent<Camera>();
            ballGenerator.inputManager = Substitute.For<InputManager>();
            ballGenerator.inputManager.IsTouchedOrClicked(out _).Returns(true);
        
            // When 
            yield return null;
        
            // Then
            var generatedObject = GameObject.Find(TestobjectName + "(Clone)");
            
            Assert.NotNull(generatedObject);
        }

        [UnityTest]
        
        public IEnumerator ShouldNotGenerateBallWhenThereIsNoTouchInput()
        {
            string TestobjectName = "TestObject3";

            BallGenerator ballGenerator = new GameObject().AddComponent<BallGenerator>();
            
            GameObject ball = new GameObject(TestobjectName);
        
            ballGenerator.ballPrefab = ball;
            ballGenerator.arCamera = new GameObject().AddComponent<Camera>();
            ballGenerator.inputManager = Substitute.For<InputManager>();
            ballGenerator.inputManager.IsTouchedOrClicked(out _).Returns(false);
        
            yield return null;
        
            var generatedObject = GameObject.Find(TestobjectName + "(Clone)");
            
            Assert.Null(generatedObject);
        
        }

        [UnityTest]

        public IEnumerator ShouldMoveTheBallOnTouchInput()
        {
            string TestobjectName = "TestObject4";
            
            BallGenerator ballGenerator = new GameObject().AddComponent<BallGenerator>();
            
            GameObject ball = new GameObject(TestobjectName);
        
            ballGenerator.ballPrefab = ball;
            ballGenerator.arCamera = new GameObject().AddComponent<Camera>();
            ballGenerator.inputManager = Substitute.For<InputManager>();
            ballGenerator.inputManager.IsTouchedOrClicked(out _).Returns(true);

            yield return null;

            var generatedObject = GameObject.Find(TestobjectName + "(Clone)");

            var position1 = generatedObject.transform.position;

            yield return new WaitForSeconds(2f);

            var position2 = generatedObject.transform.position;

            float distance = Vector3.Distance(position1, position2);
            
            Assert.NotZero(distance);

        }
        

        
    }
}