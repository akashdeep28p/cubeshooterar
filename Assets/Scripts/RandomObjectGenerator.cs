using System.Collections;
using UnityEngine;

public class RandomObjectGenerator : MonoBehaviour
{
    public GameObject targetObject;

    private const int minX = -10;
    private const int maxX = 10;
    private const int minZ = -10;
    private const int maxZ = 10;
    private const int maxObjects = 30;


    private void Start()
    {
        StartCoroutine(GenerateRandomTargetObjects());
    }

    private IEnumerator GenerateRandomTargetObjects()
    {
        int objectQuantity = 0;

        while (objectQuantity < maxObjects)
        {
            int x = Random.Range(minX, maxX);
            int z = Random.Range(minZ, maxZ);

            Instantiate(targetObject, new Vector3(x, 0, z), Quaternion.identity);

            yield return new WaitForSeconds(0.1f);
            objectQuantity++;
        }
    }
}