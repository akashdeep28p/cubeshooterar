using UnityEngine;

public class InputManager : MonoBehaviour
{
    public virtual bool IsTouchedOrClicked(out Vector2 screenPoint)
    {
        bool touched = Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began;
        bool clicked = Input.GetMouseButtonDown(0);

        if (touched)
            screenPoint = Input.touches[0].position;
        else if (clicked)
            screenPoint = Input.mousePosition;
        else
            screenPoint = Vector2.zero;

        return touched || clicked;
    }

   
}
