using UnityEngine;

public class ScoreGenerator : MonoBehaviour
{
    private readonly GUIStyle _guiStyle = new GUIStyle();
    private const int TextSize = 50;
    private const int ScoreBoxXCoordinate = 10;
    private const int ScoreBoxYCoordinate = 10;
    private const int ScoreBoxWidth = 100;
    private const int ScoreBoxHeight = 20;

    private void OnGUI()
    {
         _guiStyle.fontSize = TextSize;
        _guiStyle.normal.textColor = Color.white;
        GUI.Label(new Rect(ScoreBoxXCoordinate , ScoreBoxYCoordinate ,ScoreBoxWidth , ScoreBoxHeight) , "Score " + ChangeColorOnCollision.Score , _guiStyle);
    }
}
