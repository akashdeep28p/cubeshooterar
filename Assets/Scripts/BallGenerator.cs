﻿using UnityEngine;

public class BallGenerator : MonoBehaviour
{
    private float speed = 10f;

    public GameObject ballPrefab;
    public InputManager inputManager;
    public Camera arCamera;

    private void InstantiateBallAlongTheRaycast(Ray ray)
    {
        GameObject ball = Instantiate(ballPrefab, transform.position, Quaternion.identity);

        ball.AddComponent(typeof(Rigidbody));
        ball.AddComponent(typeof(MeshRenderer));

        ball.GetComponent<Renderer>().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        ball.GetComponent<Rigidbody>().velocity = ray.direction * speed;
    
    }

    private void Update()
    {
        if (inputManager.IsTouchedOrClicked(out Vector2 screenPoint))
        {
            Ray ray = arCamera.ScreenPointToRay(screenPoint);
            InstantiateBallAlongTheRaycast(ray);
        }
    }
}