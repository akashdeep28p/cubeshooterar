using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests.PlayModeTests
{
    public class RandomObjectGeneratorTest
    {
        private int _totalCubes = 30;
        [UnityTest]
        public IEnumerator ShouldCreateANewCubeEveryFrame()
        {
            //Given
            RandomObjectGenerator randomObjectGenerator = new GameObject().AddComponent<RandomObjectGenerator>();
            GameObject block = new GameObject("TestObject");
            randomObjectGenerator.targetObject = block;
            
            //When
            yield return null;

            //Then
            var generatedObject = GameObject.Find("TestObject(Clone)");
            Assert.NotNull(generatedObject);
        }

        [UnityTest]
        public IEnumerator ShouldCreate30Cubes()
        {
            //Given
            RandomObjectGenerator randomObjectGenerator = new GameObject().AddComponent<RandomObjectGenerator>();
            GameObject block = new GameObject("TestObject1");
            randomObjectGenerator.targetObject = block;
            yield return null;
           
            //When
            yield return new WaitForSeconds(3.5f);
            
            //Then
            int count = 0;
            foreach (var gameObject in Object.FindObjectsOfType<GameObject>())
            {
                if (gameObject.name == "TestObject1(Clone)")
                    count++;

            }
            Assert.AreEqual(_totalCubes, count);

        }
    
    }
}


